package com.example.daniel.mydndapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

public class CreateCharacterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    //all variables to do with data collection
    private EditText editTextName;
    private Switch switchGender;
    private RadioGroup radioGroupAge;
    private Spinner spinnerRace;
    private CheckBox checkBoxGood;
    private CheckBox checkBoxEvil;
    private CheckBox checkBoxNeutral;
    private CheckBox checkBoxLawful;
    private CheckBox checkBoxChaotic;
    private EditText editTextNote;
    //addition variables used to get the data from the spinner
    private String[] raceMaker;
    private String race;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_character);
        //attaches my variables to their respective ui component
        editTextName = (EditText) findViewById(R.id.inputCharacterName);
        switchGender = (Switch) findViewById(R.id.switchGender);
        radioGroupAge = (RadioGroup) findViewById(R.id.radioAge);
        spinnerRace = (Spinner) findViewById(R.id.spinnerRace);
        checkBoxGood = (CheckBox) findViewById(R.id.isGood);
        checkBoxEvil = (CheckBox) findViewById(R.id.isEvil);
        checkBoxNeutral = (CheckBox) findViewById(R.id.isNeutral);
        checkBoxLawful = (CheckBox) findViewById(R.id.isLawful);
        checkBoxChaotic = (CheckBox) findViewById(R.id.isChaotic);
        editTextNote = (EditText) findViewById(R.id.inputNotes);
        raceMaker = getResources().getStringArray(R.array.race_maker);
        //gives the spinner its options
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.race_maker, android.R.layout.simple_spinner_dropdown_item);
        spinnerRace.setAdapter(adapter);
        //adds a listener to the spinner so it is functional
        spinnerRace.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id){
        //updates the spinner and updates race for use when saving
        race = raceMaker[position];
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent){
        race = "No race selected";
    }

    //when the save button is clicked
    public void goSave(View v){
        // fuction collects the data from all the ui's feilds
        // gets the name directly from the editText
        String aName = editTextName.getText().toString();
        // gets the gender by checking the switches value
        String aGender;
        if (switchGender.isChecked()){
            aGender = "Male";
        } else {
            aGender = "Female";
        }
        // gets the age range by taking the text from the selected radioButton
        String anAge = ((RadioButton) findViewById(radioGroupAge.getCheckedRadioButtonId())).getText().toString();
        // gets the race from the variable set when the spinner is used
        String aRace = race;
        // gets the alignment by useing a series of if statements
        String anAlignment;
        if (checkBoxChaotic.isChecked()){
            anAlignment = "Chaotic";
        } else if (checkBoxLawful.isChecked()){
            anAlignment = "Lawful";
        } else {
            anAlignment = "Neutral";
        }
        if (checkBoxGood.isChecked()){
            anAlignment = anAlignment + " Good";
        } else if (checkBoxEvil.isChecked()) {
            anAlignment = anAlignment + " Evil";
        } else {
            anAlignment = anAlignment + " Neutral";
        }
        // gets the notes directly from the editText
        String aNote = editTextNote.getText().toString();
        // used by me to check consistency
        Log.d("Create character ", aName + " " + aGender + " " + aRace + " " + anAge + " " + anAlignment + " " + aNote);
        // creates a database handler to save the information to a database for later use
        DatabaseHandler db = new DatabaseHandler(this);
        db.addCharacter(new Character(aName, aGender, aRace, anAge, anAlignment, aNote));
        // lets the user know the character has been saved
        Toast.makeText(getApplicationContext(), "Your character has been saved!", Toast.LENGTH_LONG).show();
    }

}
