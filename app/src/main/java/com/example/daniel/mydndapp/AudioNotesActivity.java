package com.example.daniel.mydndapp;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.File;

public class AudioNotesActivity extends AppCompatActivity {
    //creating the variables required in the class
    private MediaPlayer mediaPlayer;    //used to play audio
    private MediaRecorder recorder;     //used to record audio
    private String OUTPUT_AUDIO_FILE;   //used to set the save path

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_notes);
        //this sets the path to the devices sd card and will give the file its name and extention
        OUTPUT_AUDIO_FILE = Environment.getExternalStorageDirectory().getAbsolutePath()+"/audiorecorded.3gpp";
    }

    //start recording button
    public void startClick(View view) throws Exception{
        //if there is a media recorder present ditch it (prevents issues)
        if(recorder != null){
            recorder.release();
        }
        //this creates a file for the audio to be saved at
        File outFile = new File(OUTPUT_AUDIO_FILE);
        //If there is data in the file delete it, prevents data corruption
        if(outFile.exists()){
            outFile.delete();
        }

        recorder = new MediaRecorder();     //makes a new media recorder
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);     //sets the audio Source
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);     //sets the audio to .3gpp
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);    //sets the encoder to narrow band
        recorder.setOutputFile(OUTPUT_AUDIO_FILE);  //sets the output to the file I made

        //Begins recording
        recorder.prepare();
        recorder.start();
        //tells the user recording has begun
        Toast.makeText(getApplicationContext(), "Recording Started", Toast.LENGTH_LONG).show();

    }
    //stop recording button
    public void finishClick(View view){
        //if something is recording stop recording
        if(recorder != null){
            recorder.stop();
            //tells the user the recording has finished
            Toast.makeText(getApplicationContext(), "Recording Ended", Toast.LENGTH_SHORT).show();
        }
    }
    //play recording button
    public void playClick(View view) throws Exception{
        //if there is media playing ditch it (prevents issues)
        if(mediaPlayer != null){
            try{
                mediaPlayer.release();
            }catch (Exception e){
                e.printStackTrace();
            }
        }


        mediaPlayer = new MediaPlayer();    //makes a new media player
        mediaPlayer.setDataSource(OUTPUT_AUDIO_FILE); //sets the media player to play the saved recording
        mediaPlayer.prepare();
        mediaPlayer.start();

        Toast.makeText(getApplicationContext(), "Audio Playing", Toast.LENGTH_LONG).show();
    }
    //stop recording button
    public void stopClick(View view){
        //if something is playing stop playing
        if(mediaPlayer != null){
            try{
                mediaPlayer.release();

                Toast.makeText(getApplicationContext(), "Audio Stoped", Toast.LENGTH_SHORT).show();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
