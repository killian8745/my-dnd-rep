package com.example.daniel.mydndapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Daniel on 12/12/2016.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    @Override
    public void onCreate(SQLiteDatabase db){
        //creates the database with the table name "characterTable" and all the character data in seperate columns
        db.execSQL("CREATE TABLE characterTable (name, gender, age, race, alignment, notes)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
    }

    public DatabaseHandler(Context context) {
        super(context, "characterDB", null, 1);
    }

    //used to add characters to the database
    public void addCharacter(Character character){
        //gets the database
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        //creates contentValues that can be used to easily add the classes data to the database
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", character.getName());
        contentValues.put("gender", character.getGender());
        contentValues.put("age", character.getAge());
        contentValues.put("race", character.getRace());
        contentValues.put("alignment", character.getAlignment());
        contentValues.put("notes", character.getNotes());
        //inserts the character data in to the database via the contentvalues
        long result = sqLiteDatabase.insert("characterTable", null, contentValues);
        //checks if the insert was successful or not
        if (result > 0){
            Log.d("dbhelper", "insert successful");
        } else{
            Log.d("dbhelper", "insert failed");
        }
        //closes the database to avoid issues
        sqLiteDatabase.close();
    }

    //used to get the number of characters in the database
    public int characterNumber(){
        //gets the database
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        //uses the cursor and a rawquery to get the number or rows in the database
        Cursor cursor = sqLiteDatabase.rawQuery("Select rowid From characterTable", null);
        cursor.moveToFirst();
        int total = cursor.getCount();
        //closes the cursor to avoid issues
        cursor.close();
        //returns the number of records stored in the databases character table
        return total;
    }
    //used to get the data about a character from the database
    public String[] viewCharacter(int id){
        //gets the database
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        //Selects all the data from the table where the id matches the character
        Cursor cursor = sqLiteDatabase.rawQuery("Select * FROM characterTable WHERE rowid == " + String.valueOf(id), null);
        //varable used to return the data
        String[] characterData = new String[6];
        //if the cursor isnt null then copy the data to the stringArray
        if (cursor.moveToFirst()){
            characterData[0] = cursor.getString(cursor.getColumnIndex("name"));
            characterData[1] = cursor.getString(cursor.getColumnIndex("gender"));
            characterData[2] = cursor.getString(cursor.getColumnIndex("age"));
            characterData[3] = cursor.getString(cursor.getColumnIndex("race"));
            characterData[4] = cursor.getString(cursor.getColumnIndex("alignment"));
            characterData[5] = cursor.getString(cursor.getColumnIndex("notes"));
        }
        //closes the cursor to avoid issues
        cursor.close();
        //Returns all the data about the character
        return characterData;
    }
}
