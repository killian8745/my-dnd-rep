package com.example.daniel.mydndapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class PhotoAidActivity extends AppCompatActivity {

    //used to update the imageView
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_aid);
        //attaches the imageView to the view in the ui
        imageView = (ImageView) findViewById(R.id.imageView3);
    }

    //used when the button is clicked
    public void takeNewPhotoClick(View v){
        //makes an intent to take a photo
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //Starts the activity
        startActivityForResult(intent, 0);
    }

    //override used so the bitmap can be intercepted and used to update the imageview
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        //makes sure its from the right activity
        if(requestCode == 0){
            Bitmap theImage = (Bitmap)data.getExtras().get("data");
            //set the imageView to the photo taken
            imageView.setImageBitmap(theImage);
        }
    }
}
