package com.example.daniel.mydndapp;

/**
 * Created by Daniel on 12/12/2016.
 */

public class Character {
    //variables used for information storage in the character class
    private String name;
    private String gender;
    private String age;
    private String race;
    private String alignment;
    private String notes;

    //constructer sets all of the data about the character
    public Character( String name, String gender, String age, String race, String alignment, String notes){
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.race = race;
        this.alignment = alignment;
        this.notes = notes;
    }

    //all the get and set methods for encapsulated access to the classes variables
    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getAge() {
        return age;
    }

    public String getRace() {
        return race;
    }

    public String getAlignment() {
        return alignment;
    }

    public String getNotes() {
        return notes;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public void setAlignment(String alignment) {
        this.alignment = alignment;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
