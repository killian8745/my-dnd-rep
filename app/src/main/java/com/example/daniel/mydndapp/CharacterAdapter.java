package com.example.daniel.mydndapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Daniel on 12/12/2016.
 *
 * used to create the list view on the ViewCharacterActivity
 */

public class CharacterAdapter extends ArrayAdapter<Character>{

    private int resource;
    private ArrayList<Character> characters;    //an array for storing all of the characters
    private Context context;
    public static int[] characterPictures = {   //an array for the "profile photos" of the characters
            R.drawable.human,
            R.drawable.dwarf,
            R.drawable.elf
    };
    // Constucter used to setup all of the fields
    public CharacterAdapter(Context context, int resource, ArrayList<Character> characters){
        super(context, resource, characters);
        this.resource = resource;
        this.characters = characters;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View v = convertView;
        try {
            if (v == null){
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = layoutInflater.inflate(resource, parent, false);
            }
            //section gets access to all of the views used in the list_item
            ImageView imageView = (ImageView) v.findViewById(R.id.imageView);
            TextView textViewName = (TextView) v.findViewById(R.id.textViewName);
            TextView textViewGender = (TextView) v.findViewById(R.id.textViewGender);
            TextView textViewAge = (TextView) v.findViewById(R.id.textViewAge);
            TextView textViewRace = (TextView) v.findViewById(R.id.textViewRace);
            TextView textViewAlignment = (TextView) v.findViewById(R.id.textViewAlignment);

            //gets the race from the class
            String race = characters.get(position).getAge();
            //uses ifs to find the race of the character to give them an apropreate picture
            if(race.equals("Human")){
                imageView.setImageResource(characterPictures[0]);
            } else if (race.equals("Dwarf")){
                imageView.setImageResource(characterPictures[1]);
            } else {
                imageView.setImageResource(characterPictures[2]);
            }
            //section sets all the text information for a character
            textViewName.setText(characters.get(position).getName());
            textViewGender.setText(characters.get(position).getGender());
            textViewAge.setText(characters.get(position).getAge());
            textViewRace.setText(characters.get(position).getRace());
            textViewAlignment.setText(characters.get(position).getAlignment());

        } catch (Exception e) {
            e.printStackTrace();
            e.getCause();
        }
        return v;
    }
}
