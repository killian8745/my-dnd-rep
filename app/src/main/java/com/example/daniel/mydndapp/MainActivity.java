package com.example.daniel.mydndapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MyMainActivity";
    private static final int REQUEST_CREATECHARACTER = 1234;
    private static final int REQUEST_VIEWCHARACTER = 5678;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    //when the option to create a character is click open its activity
    public void createCharacterClick(View view){
        //Toast.makeText(getApplicationContext(), "Create Character button clicked", Toast.LENGTH_SHORT).show();
        //Log.d(TAG, "user clicked Create Character Button");
        Intent aIntent = new Intent(this, CreateCharacterActivity.class);
        startActivityForResult(aIntent, REQUEST_CREATECHARACTER);
    }

    //when the option to view all characters is click open its activity
    public void viewCharacterClick(View view){
        //Toast.makeText(getApplicationContext(), "Veiw Character button clicked", Toast.LENGTH_SHORT).show();
        //Log.d(TAG, "user clicked View Character Button");
        Intent aIntent = new Intent(this, ViewCharacterActivity.class);
        startActivityForResult(aIntent, REQUEST_VIEWCHARACTER);
    }

    //when the option to record an audio note is click open its activity
    public void recordAudioClick(View view){
        startActivity(new Intent(this, AudioNotesActivity.class));
    }

    //when the option to take a photo aid is click open its activity
    public void takePhotoClick(View view){
        startActivity(new Intent(this, PhotoAidActivity.class));
    }
}
