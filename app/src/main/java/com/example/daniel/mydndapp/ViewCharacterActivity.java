package com.example.daniel.mydndapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ViewCharacterActivity extends AppCompatActivity {

    private ListView listView;
    private String[] character;
    private ArrayList<Character> characters = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_character);
        //used to fill the characterArray with data from the database
        generateCharacters();
        //Gets the list view from the ui
        listView = (ListView) findViewById(R.id.listViewComplex);
        //setsup the list_item using the characterAdapter
        listView.setAdapter(new CharacterAdapter(this, R.layout.list_item, characters));
        //sets an onclick listener so when the user click on a character it will show the characters notes
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                        Toast.makeText(getBaseContext(), characters.get(position).getName() + " notes: " + characters.get(position).getNotes(), Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
    //gets all the characters from the data base
    private void generateCharacters(){
        //makes the databasehandler
        DatabaseHandler db = new DatabaseHandler(this);
        //it will loop for as meny characters their are in the database. starts at 1 as the databases id starts at 1
        for (int i = 1; i <= db.characterNumber(); i++){
            //gets the stringArray of all of the current characters data
            character = db.viewCharacter(i);
            //uses the data in the string array to make a character object and add it to the array
            characters.add(new Character(character[0], character[1], character[2], character[3], character[4], character[5]));
        }
    }
}
